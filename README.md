# abnamroassesment

This Project is for test assessment by ABN AMRO

Files inclued in Projects:
  - GitLabIssueTestingAssementENV.postman_environment.json : This file holds are required ENV variables and setting to run postman collection.
  - GitLabIssueTestingAssesment.postman_collection.json : This files includes collection of postman which must be imported in POSTMAN for test execution
  - TestCases_GitLabIssuesAPI_v1.0.xlsx : Detail analysis of GitLab Issue api for testing and all testcases are mentioned in this file.
  -  test_assignment_be.txt : Assignment details for implementation, purpose and scope


Description of Postman Collection :
This collection include gitlab Issues API automated test cases for CRUD method.

Initial Setup for validation :
- Install POSTMAN if not available ([https://www.getpostman.com/downloads](https://www.getpostman.com/downloads/))
- Import ENV variables by using file "GitLabIssueTestingAssementENV.postman_environment.json"
- Import Postman collection. ([Instruction To Setup collection](https://developer.ft.com/portal/docs-start-install-postman-and-import-request-collection))

Steps to run all test cases using runner:
- Please use Iteration count to 9
- Please select "GitLabIssueTestingAssementENV" ENV during run setup
- Please refer following link for step by instruction to run suite
      https://learning.getpostman.com/docs/postman/collection_runs/starting_a_collection_run/

Two different folders are created in collection based on type of test cases:
- Happy Flow : Automated suite which cover happy scenario for Issues CRUD operation which could be used during Sanity check or minor changes to ensure availability of system
- Regression : This suite is extensive suite with combination of positive and negative test cases. So this could be used when complete regression is required for functionality

Please note that both test suites could be executed together for maximum coverage of testing.

Initiate loopCount = 1 for starting test execution from first test case. To Set loopCount, please edit ENV variables
Assertions are also added on response to identify success and failure of regression run.

Steps to execute individual api testing:
Option 1:
- Open specific API tab from collection
- Manually update loopCount in ENV for execution of specific scenario

Option 2:
- Open specific API tab from collection
- Paste following line of code at bottom of "test script" section for respective tab

/*-----Start of Snippet------*/
var count = parseInt(postman.getEnvironmentVariable("loopCount"));
if (count > 9) {
   	postman.setEnvironmentVariable("loopCount", "1");
}else{
var newCounter = count + 1;
postman.setEnvironmentVariable("loopCount", newCounter);
}
/*-----End of Snippet-------*/
- Execute same tab multiple times and different test case will get executed sequentially.


Scope of This Collection :
- "identification of different test cases for GitLab Issues CRUD API's" 
- " Scripting of each API possible critical scenario as part of API regression testing" 
- "Implementation of assertion on output on basis of behaviour of API (Behavioural Driven Testing) ".

Please refer testCases document for details about test cases.

Remark : This postman test suite cover critical and regression scenario's.
         In attached test cases sheet, manual test cases are also highlighted, which are not automated as part of this assessment. 
